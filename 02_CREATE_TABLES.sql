CREATE TABLE estado_kanban (
	id_estado_kanban     NUMBER(2) NOT NULL ,
	descripcion			 VARCHAR2(100) NOT NULL,
                CONSTRAINT ESTADO_KANBAN_PK PRIMARY KEY (id_estado_kanban)
);


CREATE TABLE estado_proyecto (
                id_estado VARCHAR2(3) NOT NULL,
                descripcion VARCHAR2(20) NOT NULL,
                CONSTRAINT ESTADO_PROYECTO_PK PRIMARY KEY (id_estado)
);


CREATE TABLE proyecto (
                id_proyecto NUMBER NOT NULL,
                nombre VARCHAR2(100) NOT NULL,
                descripcion VARCHAR2(500) NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE,
                estado VARCHAR2(3)  DEFAULT 'PEN' NOT NULL,
                CONSTRAINT PROYECTO_PK PRIMARY KEY (id_proyecto)
);

ALTER TABLE proyecto
	add constraint R_ESTADO_PROYECTO FOREIGN KEY (estado)
	REFERENCES estado_proyecto (id_estado);

alter table proyecto
  add constraint CK_ESTADO_CHEK1
  check (estado IN ('PEN', 'CAN','TER','CUR'));


CREATE TABLE sprint_backlog (
                id_sprint NUMBER NOT NULL,
                nombre VARCHAR2(100) NOT NULL,
                fecha_inicio DATE NOT NULL,
                fecha_fin DATE,
                id_proyecto NUMBER NOT NULL,
                CONSTRAINT SPRINT_BACKLOG_PK PRIMARY KEY (id_sprint)
);

ALTER TABLE sprint_backlog
	add constraint R_PROYECTO FOREIGN KEY (id_proyecto)
	REFERENCES proyecto (id_proyecto);

CREATE TABLE rol_sistema (
                id_rol NUMBER NOT NULL,
                descripcion VARCHAR2(100) NOT NULL,
                CONSTRAINT ROL_SISTEMA_PK PRIMARY KEY (id_rol)
);


CREATE TABLE permiso (
                id_permiso NUMBER NOT NULL,
                descripcion_permiso VARCHAR2(100) NOT NULL,
                edicion CHAR(2) DEFAULT 'NO' NOT NULL CONSTRAINT CK_EDICION_CHECK2 CHECK (edicion IN ('SI', 'N0')),
                visualizacion CHAR(2) DEFAULT 'NO' NOT NULL CONSTRAINT CK_VISUALIZACION_CHECK3 CHECK (visualizacion IN ('SI', 'N0')),
                borrado CHAR(2) DEFAULT 'NO' NOT NULL CONSTRAINT CK_BORRADO_CHECK4 CHECK (borrado IN ('SI', 'N0')),
                id_rol NUMBER NOT NULL,
                CONSTRAINT PERMISO_PK PRIMARY KEY (id_permiso)
);

ALTER TABLE permiso
add constraint R_ROL_SISTEMA FOREIGN KEY (id_rol)
REFERENCES rol_sistema(id_rol);

CREATE TABLE usuario (
                nombre_usuario VARCHAR2(10) NOT NULL,
                nombre VARCHAR2(100) NOT NULL,
                apellido VARCHAR2(100) NOT NULL,
                contrasenha VARCHAR2(10) NOT NULL,
                correo VARCHAR2(30) NOT NULL,
                CONSTRAINT USUARIO_PK PRIMARY KEY (nombre_usuario)
);


CREATE TABLE user_histories (
                id_us NUMBER NOT NULL,
                descripcion VARCHAR2(100) NOT NULL,
                estado_kanban NUMBER(2) NOT NULL,
                us_sprint NUMBER NOT NULL,
                usuario VARCHAR2(10) NOT NULL,
                id_rol NUMBER NOT NULL,
                CONSTRAINT USER_HISTORIES_PK PRIMARY KEY (id_us)
);


ALTER TABLE user_histories ADD CONSTRAINT R_ESTADO_KANBAN
FOREIGN KEY (estado_kanban)
REFERENCES estado_kanban (id_estado_kanban);


ALTER TABLE user_histories ADD CONSTRAINT R_SPRINT_USER_HISTORIES
FOREIGN KEY (us_sprint)
REFERENCES sprint_backlog (id_sprint);


ALTER TABLE user_histories ADD CONSTRAINT R_ROL_SISTEMA_USER_HISTORIES
FOREIGN KEY (id_rol)
REFERENCES rol_sistema (id_rol);

ALTER TABLE user_histories ADD CONSTRAINT R_USUARIO_USER_HISTORIES
FOREIGN KEY (usuario)
REFERENCES usuario (nombre_usuario);